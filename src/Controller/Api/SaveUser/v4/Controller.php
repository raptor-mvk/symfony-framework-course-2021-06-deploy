<?php

namespace App\Controller\Api\SaveUser\v4;

use App\Entity\User;
use App\Manager\UserManager;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use Symfony\Component\HttpFoundation\Response;
use App\DTO\SaveUserDTO;
use OpenApi\Annotations as OA;

class Controller extends AbstractFOSRestController
{
    private UserManager $userManager;

    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @Rest\Post("/api/v4/save-user")
     *
     * @OA\Post(
     *     operationId="addUserV4",
     *     tags={"Пользователи"},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(property="login",type="string"),
     *                 @OA\Property(property="phone",type="string"),
     *                 @OA\Property(property="email",type="string"),
     *                 @OA\Property(property="preferEmail",type="integer",pattern="0|1")
     *             )
     *         )
     *     )
     * )
     *
     * @RequestParam(name="login")
     * @RequestParam(name="password")
     * @RequestParam(name="roles")
     * @RequestParam(name="age", requirements="\d+")
     * @RequestParam(name="isActive", requirements="true|false")
     */
    public function saveUserAction(string $login, string $password, string $roles, string $age, string $isActive): Response
    {
        $userDTO = new SaveUserDTO([
                'login' => $login,
                'password' => $password,
                'roles' => json_decode($roles, true, 512, JSON_THROW_ON_ERROR),
                'age' => (int)$age,
                'isActive' => $isActive === 'true']
        );
        $userId = $this->userManager->saveUserFromDTO(new User(), $userDTO);
        [$data, $code] = ($userId === null) ? [['success' => false], 400] : [['id' => $userId], 200];
        return $this->handleView($this->view($data, $code));
    }
}
