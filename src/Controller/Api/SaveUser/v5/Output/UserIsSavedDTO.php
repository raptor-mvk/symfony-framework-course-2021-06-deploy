<?php

namespace App\Controller\Api\SaveUser\v5\Output;

use App\Entity\Traits\SafeLoadFieldsTrait;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as OA;

class UserIsSavedDTO
{
    use SafeLoadFieldsTrait;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("numeric")
     * @OA\Property(property="id", type="integer")
     */
    public int $id;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(max=32)
     * @OA\Property(property="login", type="string")
     */
    public string $login;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("numeric")
     * @OA\Property(property="age", type="integer")
     */
    public int $age;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("bool")
     * @OA\Property(property="isActive", type="boolean")
     */
    public bool $isActive;

    public function getSafeFields(): array
    {
        return ['id', 'login', 'age', 'isActive'];
    }
}
